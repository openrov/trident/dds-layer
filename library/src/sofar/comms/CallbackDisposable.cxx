#include "sofar/comms/CallbackDisposable.hpp"

namespace sofar {
namespace comms {

    CallbackDisposable::CallbackDisposable(Callback _callback) : callback(_callback) {
    }

    void CallbackDisposable::dispose() {
        callback( std::shared_ptr<IDisposable>(this) );
    }

}
}