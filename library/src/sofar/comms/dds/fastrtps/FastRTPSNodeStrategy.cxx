#include "sofar/comms/dds/fastrtps/FastRTPSNodeStrategy.hpp"
#include "sofar/comms/dds/fastrtps/FastRTPSParticipant.hpp"

#include <memory>
#include <sofar/comms/dds/fastrtps/FastRTPSNodeStrategy.hpp>
#include "sofar/comms/Node.hpp"

// #include "trident/communication/discovery/Beacon.hpp"
// #include "trident/communication/devices/lights/LightPort.hpp"

// #include <orov/msg/system/ROVBeaconPubSubTypes.h>
// #include <orov/msg/system/ROVBeacon.h>

// #include <orov/msg/device/LightPower.h>
// #include <orov/msg/device/LightPowerPubSubTypes.h>

using namespace std;

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    FastRTPSNodeStrategy::FastRTPSNodeStrategy( const std::string& nodeName,
                                const std::vector<std::string>& partitions,
                                const int& domain )

    {
        auto fastRTPSParticipant = make_shared<FastRTPSParticipant>( nodeName, domain );

        participant = fastRTPSParticipant;

    }



}
}
}
}