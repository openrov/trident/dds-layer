#include <sofar/comms/dds/fastrtps/FastRTPSPort.hpp>
#include <sofar/comms/dds/fastrtps/FastRTPSReaderTopicConfiguration.hpp>

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    void FastRTPSPort::activate() {
        for (auto &configuration : readerConfigurations) {
            auto reader = configuration->createReader(participant, partition);
            readers.insert( make_pair(configuration, reader) );
        }

        for (auto &configuration : writerConfigurations) {
            auto writer = configuration->createWriter(participant, partition);
            writers.insert( make_pair(configuration, writer) );
        }
    }

    void FastRTPSPort::deactivate() {}

    void FastRTPSPort::writeToTopic(const string &topic, void* data) {

        for (auto& config : writerConfigurations) {
            if (config->topicName == topic) {

                writers.at(config)->writeToTopic(topic, data);
            }
        }

    }

}
}
}
}
