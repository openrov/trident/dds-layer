#include <sofar/comms/dds/fastrtps/FastRTPSWriter.hpp>

#include <fastrtps/publisher/Publisher.h>
#include <spdlog/spdlog.h>

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    void FastRTPSWriter::writeToTopic(const std::string &topic, void* data) const {

        // DEBUG
        // auto* lp = (orov::msg::device::LightPower*)data;
        // spdlog::info("## SENDING LP {} {}", lp->power(), publisher->getAttributes().qos.m_partition.getNames().at(0));

        spdlog::info("Topic: {}", publisher->getAttributes().topic.topicName);
        publisher->write(data);

    }
}
}
}
}