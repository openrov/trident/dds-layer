#include "sofar/comms/dds/fastrtps/FastRTPSParticipant.hpp"
#include "sofar/comms/dds/fastrtps/FastRTPSNodeStrategy.hpp"

#include <fastrtps/Domain.h>
#include <fastrtps/participant/Participant.h>
#include <fastrtps/attributes/ParticipantAttributes.h>
#include <fastrtps/participant/ParticipantListener.h>
#include <fastrtps/attributes/SubscriberAttributes.h>
#include <fastrtps/attributes/PublisherAttributes.h>
#include <fastrtps/publisher/Publisher.h>
#include <fastrtps/publisher/PublisherListener.h>
#include <fastrtps/TopicDataType.h>

#include <sofar/comms/dds/fastrtps/FastRTPSSubsccriptionListener.hpp>
#include <sofar/comms/dds/fastrtps/FastRTPSParticipant.hpp>
#include <sofar/comms/dds/fastrtps/FastRTPSReader.hpp>
#include <sofar/comms/dds/fastrtps/FastRTPSWriter.hpp>
#include <sofar/comms/dds/fastrtps/FastRTPSPort.hpp>


#include <boost/any.hpp>
#include <functional>

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    using namespace eprosima::fastrtps;
    using namespace eprosima::fastrtps::rtps;

    class PublisherListener : public eprosima::fastrtps::PublisherListener {
    public:
        PublisherListener() = default;

        void onPublicationMatched(Publisher *pub, rtps::MatchingInfo &info) override {
            (void)pub, (void)info;
            spdlog::info("@ [FASTRTPS] Publication matched for topic {}", pub->getAttributes().topic.topicName);
        }
    };

    class ParticipantListener : public eprosima::fastrtps::ParticipantListener {
    public:
        ParticipantListener() {}

        void onSubscriberDiscovery(fastrtps::Participant *participant, rtps::ReaderDiscoveryInfo &&info) override {
            (void)participant, (void)info;

//            switch(info.status)  {
//                case eprosima::fastrtps::rtps::ReaderDiscoveryInfo::DISCOVERED_READER:
//                    spdlog::info("Discovered Reader for topicName {}", info.info.topicName());
//            }
        }

//        void onSubscriberDiscovery(Participant* participant, rtps::ReaderDiscoveryInfo&& info) override {
//            (void)participant, (void)info;
//
//            switch(info.status)  {
//                case eprosima::fastrtps::rtps::ReaderDiscoveryInfo::DISCOVERED_READER:
//                    spdlog::info("Discovered Reader for topicName {}", info.info.topicName());
//            }
//        }

        void onPublisherDiscovery(
                eprosima::fastrtps::Participant *participant,
                eprosima::fastrtps::rtps::WriterDiscoveryInfo &&info) override {
            (void)participant;
            switch(info.status) {
                case eprosima::fastrtps::rtps::WriterDiscoveryInfo ::DISCOVERED_WRITER:
//                    spdlog::info("Discovered Writer for topicName {}", info.info.topicName());
                    /* Process the case when a new publisher was found in the domain */
//                     if ( info.info.topicName() == "rov_beacon" ) {
//                         WriterProxyData data(4u, 1u);
// //                        cout << "GUID: " << info.info.guid() << endl;
//                         if (participant->get_remote_writer_info(info.info.guid(), data)) {
//                             std::vector<std::string> ipAddresses;
//                             for ( auto& item : info.info.remote_locators().unicast) {
//                                 const octet *address = item.address;
//                                 std::ostringstream addressStream;
//                                 addressStream<< (int)address[12] << "." << (int)address[13]
//                                     << "." << (int)address[14] << "." << (int)address[15];
//                                 ipAddresses.push_back(addressStream.str());
//                             }
//                             std::ostringstream guid; guid << info.info.guid();
// //                            callback(guid.str(), ipAddresses);
//                         }
//                         // Could not get writer info
//                     }
                    break;
            }

        }

    private:

    };

    FastRTPSParticipant::FastRTPSParticipant(  const std::string& participantName, const uint32_t& domainID )  {

        eprosima::fastrtps::ParticipantAttributes attributes;
        attributes.rtps.builtin.discovery_config.discoveryProtocol = eprosima::fastrtps::rtps::DiscoveryProtocol::SIMPLE;
        attributes.rtps.builtin.discovery_config.use_SIMPLE_EndpointDiscoveryProtocol = true;
        attributes.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
        attributes.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;
        attributes.rtps.builtin.readerHistoryMemoryPolicy = eprosima::fastrtps::rtps::MemoryManagementPolicy::PREALLOCATED_WITH_REALLOC_MEMORY_MODE;
        attributes.rtps.builtin.domainId = domainID;
//        attributes.rtps.builtin.leaseDuration = eprosima::fastrtps::c_TimeInfinite;
        attributes.rtps.setName( participantName.c_str() );

        // TODO Add the listener again to gain access to the participant match functions (To get IP Address)
        //    auto *listener = new trident::communication::discovery::fastrtps::ROVBeaconParticipantListener(
//            [&](std::string writerGuid, std::vector<std::string> ipAddresses) {
//               for (auto& address : ipAddresses) {
//                   cout << address << endl;
//               }
//            });
        auto *listener = new ParticipantListener();
        participant = eprosima::fastrtps::Domain::createParticipant(attributes, listener);
//        participant = eprosima::fastrtps::Domain::createParticipant(attributes);

    }


    eprosima::fastrtps::Subscriber* FastRTPSParticipant::createSubscriber(
            const std::string &topicName,
            const qos::QosType& qosType,
            const std::string& partition,
            const std::shared_ptr<eprosima::fastrtps::TopicDataType> dataType,
            eprosima::fastrtps::SubscriberListener *listener) const {

        registerType(dataType);

        SubscriberAttributes subscribeAttributes;
        subscribeAttributes.topic.topicKind = WITH_KEY;
        subscribeAttributes.topic.topicDataType = dataType->getName();
        subscribeAttributes.topic.topicName = topicName;

        // TODO put into XML
        if (qosType == qos::QosType::BestEffort) {
            subscribeAttributes.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
            subscribeAttributes.topic.historyQos.depth = 100;
            subscribeAttributes.qos.m_reliability.kind = BEST_EFFORT_RELIABILITY_QOS;
            subscribeAttributes.qos.m_durability.kind = VOLATILE_DURABILITY_QOS;
            subscribeAttributes.qos.m_disablePositiveACKs.enabled = true;
            subscribeAttributes.qos.m_disablePositiveACKs.hasChanged = true;
            spdlog::info("DisablePositiveACK 4: {}", subscribeAttributes.qos.m_disablePositiveACKs.enabled);

        } else if (qosType == qos::QosType::KeepLastReliable) {
            subscribeAttributes.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
            subscribeAttributes.topic.historyQos.depth = 1;
            subscribeAttributes.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
            subscribeAttributes.qos.m_durability.kind = VOLATILE_DURABILITY_QOS;
            subscribeAttributes.qos.m_disablePositiveACKs.enabled = true;
            subscribeAttributes.qos.m_disablePositiveACKs.hasChanged = true;
            spdlog::info("DisablePositiveACK 5: {}", subscribeAttributes.qos.m_disablePositiveACKs.enabled);

        } else if (qosType == qos::QosType::KeepLastReliableTransientLocal) {
            subscribeAttributes.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
            subscribeAttributes.topic.historyQos.depth = 1;
            subscribeAttributes.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
            subscribeAttributes.qos.m_durability.kind = TRANSIENT_LOCAL_DURABILITY_QOS;
            subscribeAttributes.qos.m_disablePositiveACKs.enabled = true;
            subscribeAttributes.qos.m_disablePositiveACKs.hasChanged = true;
            spdlog::info("DisablePositiveACK 6: {}", subscribeAttributes.qos.m_disablePositiveACKs.enabled);
        }


        subscribeAttributes.qos.m_partition.clear();
        for( auto &part : getPartitions(partition) ) {
            subscribeAttributes.qos.m_partition.push_back(part.c_str());
        }

        try {
            auto res = Domain::createSubscriber(participant, subscribeAttributes, listener);
            spdlog::info("DisablePositiveACK X: {}", res->getAttributes().qos.m_disablePositiveACKs.enabled);

            return res;
        } catch (std::exception& err) {
            spdlog::error("Error creating subscriber: {}", err.what());
        }

    }


    eprosima::fastrtps::Publisher* FastRTPSParticipant::createPublisher(
            const std::string &topicName,
            const qos::QosType& qosType,
            const std::string& partition,
            const std::shared_ptr<eprosima::fastrtps::TopicDataType> dataType) const {

        registerType(dataType);

        PublisherAttributes publisherAttributes;
        publisherAttributes.topic.topicKind = WITH_KEY;
        publisherAttributes.topic.topicDataType = dataType->getName();
        publisherAttributes.topic.topicName = topicName;

        // TODO put into XML
        if (qosType == qos::QosType::BestEffort) {
            publisherAttributes.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
            publisherAttributes.topic.historyQos.depth = 100;
            publisherAttributes.qos.m_reliability.kind = BEST_EFFORT_RELIABILITY_QOS;
            publisherAttributes.qos.m_durability.kind = VOLATILE_DURABILITY_QOS;
            publisherAttributes.qos.m_disablePositiveACKs.enabled = true;
            publisherAttributes.qos.m_disablePositiveACKs.duration = 1;
            publisherAttributes.qos.m_disablePositiveACKs.hasChanged = true;
            spdlog::info("DisablePositiveACK 1: {}", publisherAttributes.qos.m_disablePositiveACKs.enabled);

        } else if (qosType == qos::QosType::KeepLastReliable) {
            publisherAttributes.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
            publisherAttributes.topic.historyQos.depth = 1;
            publisherAttributes.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
            publisherAttributes.qos.m_durability.kind = VOLATILE_DURABILITY_QOS;
            publisherAttributes.qos.m_disablePositiveACKs.enabled = true;
            publisherAttributes.qos.m_disablePositiveACKs.duration = 1;
            publisherAttributes.qos.m_disablePositiveACKs.hasChanged = true;
            spdlog::info("DisablePositiveACK 2: {}", publisherAttributes.qos.m_disablePositiveACKs.enabled);

        } else if (qosType == qos::QosType::KeepLastReliableTransientLocal) {
            publisherAttributes.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
            publisherAttributes.topic.historyQos.depth = 1;
            publisherAttributes.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
            publisherAttributes.qos.m_durability.kind = TRANSIENT_LOCAL_DURABILITY_QOS;
            publisherAttributes.qos.m_disablePositiveACKs.enabled = true;
            publisherAttributes.qos.m_disablePositiveACKs.duration = 1;
            publisherAttributes.qos.m_disablePositiveACKs.hasChanged = true;
            spdlog::info("DisablePositiveACK 3: {}", publisherAttributes.qos.m_disablePositiveACKs.enabled);
        }

        publisherAttributes.qos.m_partition.clear();
        for( auto &part : getPartitions(partition) ) {
            publisherAttributes.qos.m_partition.push_back(part.c_str());
        }

        try {
            auto* listener = new PublisherListener();
            auto res = Domain::createPublisher(participant, publisherAttributes, listener);
            spdlog::info("DisablePositiveACK Z: {}", res->getAttributes().qos.m_disablePositiveACKs.enabled);
            return res;
        } catch (std::exception& err) {
            spdlog::error("Error creating Publisher: {}", err.what());
        }

    }



    std::shared_ptr<DataReader> FastRTPSParticipant::createReader(
            const std::string &topicName,
            const qos::QosType &qosType,
            const std::string &partition,
            const std::shared_ptr<eprosima::fastrtps::TopicDataType> dataType,
            std::shared_ptr<FastRTPSSubscribeListenerBase> listener
            ) {

        auto subscriber = createSubscriber(
                topicName,
                qosType,
                partition,
                dataType,
                listener.get());
        return std::make_shared<FastRTPSReader>(subscriber, listener);

    }
    std::shared_ptr<DataWriter> FastRTPSParticipant::createWriter(
            const std::string &topicName,
            const qos::QosType &qosType,
            const std::string &partition,
            const std::shared_ptr<eprosima::fastrtps::TopicDataType> dataType
            ) {

        auto publisher = createPublisher(
                topicName,
                qosType,
                partition,
                dataType
                );
        return std::make_shared<FastRTPSWriter>(publisher, participant);

    }

    const std::shared_ptr<PortStrategy> FastRTPSParticipant::createPort(const std::string& partition) {
        shared_ptr<Participant> self = std::shared_ptr<Participant>(this);

        const auto port = std::make_shared<FastRTPSPort>( self, partition );
        return static_pointer_cast<PortStrategy>( port );
    }

    const std::shared_ptr<eprosima::fastrtps::TopicDataType> &FastRTPSParticipant::getTopicType(const std::type_info &info) {
        return typeMap.at(info.name());
    }

    vector<string> FastRTPSParticipant::getPartitions(const string &partition) const {
        if (partition == "*" ) {
            return vector<string> { "", "*" };
        } else {
            return vector<string> { partition };
        }
    }

    void FastRTPSParticipant::registerType(const std::shared_ptr<eprosima::fastrtps::TopicDataType> dataType) const {
        TopicDataType* type;
        auto name = dataType->getName();
        if (! Domain::getRegisteredType(participant, name, &type) ) {
            Domain::registerType(participant, dataType.get());
        }
    }
}
}
}
}
