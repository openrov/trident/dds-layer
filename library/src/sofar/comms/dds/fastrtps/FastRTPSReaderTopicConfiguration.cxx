#include "sofar/comms/dds/fastrtps/FastRTPSReaderTopicConfiguration.hpp"
#include "sofar/comms/dds/fastrtps/FastRTPSParticipant.hpp"

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    shared_ptr<DataReader> FastRTPSReaderTopicConfiguration::createReader(shared_ptr<Participant> participant, const std::string& partition) {
        return static_pointer_cast<FastRTPSParticipant>(participant)
                ->createReader(
                        topicName,
                        qualityOfService,
                        partition,
                        dataType,
                        listener
                );
        ;
    }
}
}
}
}