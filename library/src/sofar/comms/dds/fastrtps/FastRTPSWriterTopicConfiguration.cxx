#include "sofar/comms/dds/fastrtps/FastRTPSWriterTopicConfiguration.hpp"
#include "sofar/comms/dds/fastrtps/FastRTPSParticipant.hpp"

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    shared_ptr<DataWriter> FastRTPSWriterTopicConfiguration::createWriter(shared_ptr<Participant> participant, const std::string& partition) {
        return static_pointer_cast<FastRTPSParticipant>(participant)
                ->createWriter(
                        topicName,
                        qualityOfService,
                        partition,
                        dataType
                );
        ;
    }
}
}
}
}