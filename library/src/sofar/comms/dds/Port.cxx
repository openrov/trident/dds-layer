
#include <sofar/comms/dds/Port.hpp>
#include <spdlog/spdlog.h>

namespace sofar {
namespace comms {
namespace dds {

    Port::Port(const shared_ptr<Participant> _participant, const std::string& partition)
    : participant(_participant)
    , partition ( partition ) {
    }

}
}
}
