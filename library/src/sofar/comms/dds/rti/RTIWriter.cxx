
#include <sofar/comms/dds/rti/RTIWriter.hpp>
#include <sofar/comms/dds/rti/RTIParticipant.hpp>

namespace sofar {
namespace comms {
namespace dds {
namespace rtix {

    rtidds::domain::DomainParticipant *RTIWriterBase::getDomainParticipant() {
        auto rtiParticipant = static_pointer_cast<RTIParticipant>(participant);
        return rtiParticipant->getDefaultParticipant();

    }

    rtidds::pub::Publisher* RTIWriterBase::getPublisher(const std::string &_partition) {
        auto rtiParticipant = static_pointer_cast<RTIParticipant>(participant);
        return rtiParticipant->getPublisher(_partition);
    }

    const rtidds::pub::qos::DataWriterQos RTIWriterBase::getWriterQosConfiguration(const qos::QosType qos) {
        auto rtiParticipant = static_pointer_cast<RTIParticipant>(participant);
        return rtiParticipant->getWriterQosConfiguration(qos);
    }


}
}
}
}