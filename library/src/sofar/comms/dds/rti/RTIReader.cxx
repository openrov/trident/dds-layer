
#include <sofar/comms/dds/rti/RTIReader.hpp>
#include <sofar/comms/dds/rti/RTIParticipant.hpp>

namespace sofar {
namespace comms {
namespace dds {
namespace rtix {

    void RTIReaderBase::setWaitSet(std::shared_ptr<rtidds::core::cond::WaitSet> _waitSet) {
        waitSet = std::move( _waitSet );
        auto rc = readCondition.get();
        waitSet->attach_condition(*rc);

    }

    rtidds::domain::DomainParticipant *RTIReaderBase::getDomainParticipant() {
        auto rtiParticipant = static_pointer_cast<RTIParticipant>(participant);
        return rtiParticipant->getDefaultParticipant();

    }

    rtidds::sub::Subscriber* RTIReaderBase::getSubscriber(const std::string &_partition) {
        auto rtiParticipant = static_pointer_cast<RTIParticipant>(participant);
        return rtiParticipant->getSubscriber(_partition);
    }

    const rtidds::sub::qos::DataReaderQos RTIReaderBase::getReaderQosConfiguration(const qos::QosType qos) {
        auto rtiParticipant = static_pointer_cast<RTIParticipant>(participant);
        return rtiParticipant->getReaderQosConfiguration(qos);
    }


}
}
}
}