#include "sofar/comms/dds/rti/RTINodeStrategy.hpp"
#include "sofar/comms/dds/rti/RTIParticipant.hpp"
#include "sofar/comms/Node.hpp"

#include <dds/domain/DomainParticipant.hpp>

#include <memory>
#include <ndds/ndds_cpp.h>


using namespace std;

namespace sofar {
namespace comms {
namespace dds {
namespace rtix {

    class MyDomainParticipantListener :
        public rtidds::domain::NoOpDomainParticipantListener {
    public:
        void on_publication_matched(rtidds::pub::AnyDataWriter &writer,
                                    const rtidds::core::status::PublicationMatchedStatus &status) override {

            spdlog::info("@ [RTI] Publication matched for topic: {}", writer.topic_name());

        }

        void on_subscription_matched(rtidds::sub::AnyDataReader &the_reader,
                                     const rtidds::core::status::TSubscriptionMatchedStatus<rti::core::status::SubscriptionMatchedStatus> &status) override {
            spdlog::info("@ [RTI] Subscription matched for topic: {}", the_reader.topic_name());
        }

    };

    RTINodeStrategy::RTINodeStrategy( const std::string& nodeName,
                                      const std::vector<std::string>& partitions,
                                      const int& domain ) :
                                        qosFilePaths { "/opt/openrov/share/orovmsg/qos/v1/openrov.xml" }
                                        , defaultLib { "OpenROV" }
                                        , defaultProfile { "Participant.Vehicle" }
                                        , domainParticipant{ nullptr }
   {

        SetupQos( qosFilePaths, defaultLib, defaultProfile );

        // Create participant
        auto partQos = rtidds::core::QosProvider::Default()->participant_qos();
        partQos.policy<rti::core::policy::EntityName>().name( nodeName );

        MyDomainParticipantListener* participant_listener =
               new MyDomainParticipantListener();
        domainParticipant = rtidds::domain::DomainParticipant( domain, partQos, participant_listener, rtidds::core::status::StatusMask::all() );

        // Set up default publisher and subscriber QOS with partitions
        auto pubQos = domainParticipant.default_publisher_qos();
        auto subQos = domainParticipant.default_subscriber_qos();

        // Fix empty partition list and invalid wildcard specification
        if( partitions.empty() || ( partitions.size() == 1 && partitions[ 0 ] == "*" ) )
        {
            pubQos << rtidds::core::policy::Partition{ std::vector<std::string>{ "", "*" } };
            subQos << rtidds::core::policy::Partition{ std::vector<std::string>{ "", "*" } };
        }
        else
        {
            pubQos << rtidds::core::policy::Partition( partitions );
            subQos << rtidds::core::policy::Partition( partitions );
        }

        domainParticipant.default_publisher_qos( pubQos );
        domainParticipant.default_subscriber_qos( subQos );
        rtidds::pub::Publisher pub = rtidds::pub::Publisher( domainParticipant );
        rtidds::sub::Subscriber sub = rtidds::sub::Subscriber( domainParticipant );


        auto rtiParticipant = make_shared<RTIParticipant>(
                &domainParticipant,
                pub,
                sub,
                nodeName,
                domain,
                qosFilePaths
                );
        participant = rtiParticipant;

    }

    void RTINodeStrategy::SetupQos(    const std::vector<std::string>& qosFilePaths,
                            const std::string& defaultLib,
                            const std::string& defaultProfile )
    {
        // Load Qos Profiles and set default library and profile
        if( !qosFilePaths.empty() )
        {

            rti::core::QosProviderParams params;
            params.url_profile( qosFilePaths );

            rtidds::core::QosProvider::Default()->default_provider_params( params );

            if(!defaultLib.empty() && !defaultProfile.empty())
            {
                rtidds::core::QosProvider::Default()->default_library( defaultLib );
                rtidds::core::QosProvider::Default()->default_profile( defaultProfile );
            }

        }
    }


}
}
}
}