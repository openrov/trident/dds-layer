#include <utility>


#include <sofar/comms/dds/rti/RTIParticipant.hpp>

#include "sofar/comms/dds/rti/RTIPort.hpp"
#include "sofar/comms/dds/rti/RTIReader.hpp"


namespace sofar {
namespace comms {
namespace dds {
namespace rtix {


    RTIParticipant::RTIParticipant(
            rtidds::domain::DomainParticipant *_participant,
            rtidds::pub::Publisher pub,
            rtidds::sub::Subscriber sub,
            const std::string &participantName,
            const uint32_t &domainID,
            vector<string> _qosFilePaths
            )
            : domainParticipant(_participant)
            , defaultPublisher(pub)
            , defaultSubscriber(sub)
            , qosFilePaths(std::move(_qosFilePaths))
            {
    }

    const std::shared_ptr<PortStrategy> RTIParticipant::createPort(const std::string &partition) {
        shared_ptr<Participant> self = std::shared_ptr<Participant>(this);

        const auto port = std::make_shared<RTIPort>(self, partition);
        return static_pointer_cast<PortStrategy>(port);
    }



    /* For some reason, the main call to loading the QOS from file seems not to work when were down the track.
     * So we have to call it again.*/
    const rtidds::sub::qos::DataReaderQos RTIParticipant::getReaderQosConfiguration(const qos::QosType qosType) {
        rti::core::QosProviderParams params;
        params.url_profile( qosFilePaths );
        rtidds::core::QosProvider::Default()->default_provider_params( params );

        string qosString;
        if (qosType == qos::QosType::BestEffort) {
            qosString = "OpenROV::Generic.BestEffort";

        } else if (qosType == qos::QosType::KeepLastReliable) {
            qosString = "OpenROV::Generic.KeepLastReliable";

        } else if (qosType == qos::QosType::KeepLastReliableTransientLocal) {
            qosString = "OpenROV::Generic.KeepLastReliable.TransientLocal";
        }
        return rtidds::core::QosProvider::Default()->datareader_qos(qosString );

    }

    /* For some reason, the main call to loading the QOS from file seems not to work when were down the track.
     * So we have to call it again.*/
    const rtidds::pub::qos::DataWriterQos RTIParticipant::getWriterQosConfiguration(const qos::QosType qosType) {
        rti::core::QosProviderParams params;
        params.url_profile( qosFilePaths );
        rtidds::core::QosProvider::Default()->default_provider_params( params );

        string qosString;
        if (qosType == qos::QosType::BestEffort) {
            qosString = "OpenROV::Generic.BestEffort";

        } else if (qosType == qos::QosType::KeepLastReliable) {
            qosString = "OpenROV::Generic.KeepLastReliable";

        } else if (qosType == qos::QosType::KeepLastReliableTransientLocal) {
            qosString = "OpenROV::Generic.KeepLastReliable.TransientLocal";
        }
        return rtidds::core::QosProvider::Default()->datawriter_qos(qosString );
    }


}

}
}
}
