
#include <sofar/comms/dds/rti/RTIPort.hpp>
#include <memory>

#include <boost/any.hpp>
#include <sofar/comms/dds/rti/RTIReader.hpp>
#include <sofar/comms/dds/rti/RTIReaderTopicConfiguration.hpp>
#include <sofar/comms/dds/PortStrategy.hpp>

namespace sofar {
namespace comms {
namespace dds {
namespace rtix {

    RTIPort::RTIPort(shared_ptr<Participant> _participant, const std::string &partition) : PortStrategy( std::move(_participant), partition) {

    }

    void RTIPort::activate() {
        for (auto &configuration : readerConfigurations) {
            auto reader = configuration->createReader(participant, partition);
            auto rtiReader = static_pointer_cast<RTIReaderBase>(reader);
            rtiReader->setWaitSet(waitSet);
            readers.insert( make_pair(configuration, reader) );
        }
        for (auto &configuration : writerConfigurations) {
            auto writer = configuration->createWriter(participant, partition);
            auto rtiWriter = static_pointer_cast<RTIWriterBase>(writer);
            writers.insert( make_pair(configuration, writer) );
        }

        initThread();
    }

    void RTIPort::deactivate() {
        terminateThreads();
        //todo cleamup readers and conditions
    }


    void RTIPort::initThread() {
        try
        {
            isRunning = true;
            readerThread = std::thread([&] ()
               {
                   while(isRunning)
                   {
                       waitSet->dispatch(rtidds::core::Duration::from_millisecs(250));
                   }
               });

        }
        catch(const std::exception& e)
        {
            //qDebug() << "Failed to start threads for: " << QString::fromStdString(m_name);
        }
    }


    void RTIPort::terminateThreads() {
        try
        {
            isRunning = false;
            readerThread.join();
        }
        catch(const std::exception& e)
        {
            //qDebug() << "No threads need to be stopped";
        }
    }

    void RTIPort::writeToTopic(const string &topic, void *data) {
        for (auto& config : writerConfigurations) {
            if (config->topicName == topic) {

                writers.at(config)->writeToTopic(topic, data);
            }
        }
    }

}
}
}
}

