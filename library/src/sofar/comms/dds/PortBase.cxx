
#include <sofar/comms/dds/PortBase.hpp>

namespace sofar {
namespace comms {
namespace dds {

    PortBase::PortBase(
            const shared_ptr<Participant> _participant,
            const shared_ptr<PortStrategy> _port)
            : participant(_participant)
            , port( _port ) {

    }


    vector<shared_ptr<ReaderTopicConfiguration>> PortBase::getReaderTopicConfigurations() {
        if (readerTopicConfigurations.empty()) {
            auto configs = createReaderTopicConfigurations();
            readerTopicConfigurations.insert(std::end(readerTopicConfigurations), std::begin(configs), std::end(configs));
        }
        return readerTopicConfigurations;
    }

    vector<shared_ptr<WriterTopicConfiguration>> PortBase::getWriterTopicConfigurations() {
        if (writerTopicConfigurations.empty()) {
            auto configs = createWriterTopicConfigurations();
            writerTopicConfigurations.insert(std::end(writerTopicConfigurations), std::begin(configs), std::end(configs));
        }
        return writerTopicConfigurations;
    }


        void PortBase::activate() {
            port->setReaderConfigurations( getReaderTopicConfigurations() );
            port->setWriterConfigurations( getWriterTopicConfigurations() );
            port->activate();
        }
    }
}
}
