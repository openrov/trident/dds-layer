#include <sofar/comms/Logging.hpp>

#include <spdlog/async.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/details/registry.h>

#include <boost/filesystem.hpp>

namespace sofar {
namespace comms {

    std::shared_ptr<spdlog::logger> CreateProductionLogger( const std::string& logName,
                                                            const std::string& logDirectory, 
                                                            uint32_t maxLogSizeBytes, 
                                                            uint32_t maxLogCount )
    {
        // Create log directory if it doesn't exist already
        boost::system::error_code ec;
        if( !boost::filesystem::create_directories( logDirectory, ec ) )
        {
            // Only throw if there was actually an error. Directories could have already existed.
            if( ec.value() !=  boost::system::errc::success )
            {
                throw std::runtime_error( std::string( "Could not create log directory: \"" + logDirectory + "\"" ) );
            }
        }

        auto path = boost::filesystem::path( logDirectory ) / boost::filesystem::path( logName );

        // NOTE: Workaround for spdlog bug where global thread pool is not default initialized when creating an async logger
        if( nullptr == spdlog::details::registry::instance().get_tp() )
        {
            spdlog::init_thread_pool( 8192, 1 );
        }

        // Create multiple sinks
        auto stdout_sink    = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        auto rot_file_sink  = std::make_shared<spdlog::sinks::rotating_file_sink_mt>( path.string(), maxLogSizeBytes, maxLogCount );
        std::vector<spdlog::sink_ptr> sinks{ stdout_sink, rot_file_sink };

        // Create logger from multiple sinks
        auto logger = std::make_shared<spdlog::async_logger>( logName, 
            std::begin( sinks ), 
            std::end( sinks ), 
            spdlog::thread_pool(), 
            spdlog::async_overflow_policy::overrun_oldest );

        // Set to flush on error or higher
        logger->flush_on( spdlog::level::err ); 

        // Set default level of logger and all sinks to trace. 
        // This allows simple configuration of log level at the logger level.
        logger->set_level( spdlog::level::info  );
        for( auto& sink : logger->sinks() )
        {
            sink->set_level( spdlog::level::trace );
        }

        // Register for access anywhere
        spdlog::register_logger( logger );

        // Set the default global logger
        spdlog::set_default_logger( logger );   

        return logger;
    }
}
}
