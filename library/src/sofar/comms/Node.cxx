#include <sofar/comms/Node.hpp>

namespace sofar {
namespace comms {

    Node::Node( 
        const std::string &nodeName, 
        const std::vector<std::string> &defaultPartitions, 
        int domain, 
        std::function<void(std::shared_ptr<sofar::comms::dds::Participant>)> participantInitCallback )
     : nodeStrategy {
            nodeName,
            defaultPartitions,
            domain
        }
    {
        participantInitCallback(nodeStrategy.defaultParticipant());
    }

    std::shared_ptr<sofar::comms::dds::Participant> Node::defaultParticipant() const {
        return nodeStrategy.defaultParticipant();
    }

}
}