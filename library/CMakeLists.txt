cmake_minimum_required(VERSION 3.10)

# Conan setup
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

if (USE_FASTRTPS)
    add_definitions(-DUSE_FASTRTPS)
    message("@@@@@@@@ FASTRTPS @@@@@@@@@@")
endif()
if (USE_RTI)
    add_definitions(-DUSE_RTI)
    message("#####  RTI  ##################")
endif()

#option(BUILD_SHARED_LIBS "Create shared libraries by default" ON)

project(sofar-dds-layer LANGUAGES CXX)

# Collect sources
file(GLOB_RECURSE SOURCES "src/**.cxx" )

if (USE_FASTRTPS)
    set(EXTRA_LIBRARIES CONAN_PKG::fastrtps CONAN_PKG::foonathan-memory  )
    list(FILTER SOURCES EXCLUDE REGEX ".*/rti/.*")
endif()
if (USE_RTI)
    set(EXTRA_LIBRARIES CONAN_PKG::rticonnextpro )
    list(FILTER SOURCES EXCLUDE REGEX ".*/fastrtps/.*")
endif()


# Create library
# add_library(${PROJECT_NAME} ${SOURCES})
#if (BUILD_SHARED_LIBS)
#    add_library( ${PROJECT_NAME} SHARED ${SOURCES} )
#else()
#    add_library( ${PROJECT_NAME} STATIC ${SOURCES} )
#endif()
add_library( ${PROJECT_NAME} ${SOURCES} )


target_include_directories( ${PROJECT_NAME}
    PUBLIC
        "include"
)

target_compile_features( ${PROJECT_NAME}
    PRIVATE
        cxx_std_14
)

# Library dependencies
# set(EXTRA_LIBRARIES "ssl" "crypt" "dl")
target_link_libraries(${PROJECT_NAME}
        PRIVATE
        CONAN_PKG::boost
        CONAN_PKG::spdlog
        ${EXTRA_LIBRARIES}
        )