#pragma once

#include <string>
#include <memory>

#include <spdlog/spdlog.h>

namespace sofar {
namespace comms {

    constexpr uint32_t LOG_DEFAULT_MAX_SIZE     = 15 * 1000 * 1000; // 15 MB
    constexpr uint32_t LOG_DEFAULT_MAX_COUNT    = 3;

    std::shared_ptr<spdlog::logger> CreateProductionLogger( const std::string& logName,
                                                            const std::string& logDirectory, 
                                                            uint32_t maxLogSizeBytes = LOG_DEFAULT_MAX_SIZE, 
                                                            uint32_t maxLogCount = LOG_DEFAULT_MAX_COUNT );
}
}