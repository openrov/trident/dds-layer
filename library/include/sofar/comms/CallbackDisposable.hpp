#pragma once

#include <functional>
#include <memory>
#include "IDisposable.hpp"

namespace sofar {
namespace comms {

    typedef std::function<void(std::shared_ptr<IDisposable>)> Callback;

    class CallbackDisposable : public IDisposable {
    public:
        CallbackDisposable(Callback);
        void dispose();

    private:
        Callback callback = nullptr;
    };

}
}