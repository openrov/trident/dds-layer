#pragma once

#include <string>

#include "sofar/comms/Qos.hpp"
#include "Participant.hpp"
#include "DataReader.hpp"

#include <boost/any.hpp>
#include <spdlog/spdlog.h>

namespace sofar {
namespace comms {
namespace dds {

    class NodeStrategy {
    protected:
        NodeStrategy() {}

    public:
        std::shared_ptr<sofar::comms::dds::Participant> defaultParticipant() const { return participant; }

    protected:
        std::shared_ptr<sofar::comms::dds::Participant> participant;
    };

}
}
}