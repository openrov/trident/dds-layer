#pragma once

#ifdef USE_FASTRTPS

#include <spdlog/spdlog.h>
#include "../PortStrategy.hpp"

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    class FastRTPSPort : public PortStrategy {
    public:
        FastRTPSPort(shared_ptr<Participant> _participant, const std::string& partition) : PortStrategy(_participant, partition) {
        } ;
        void activate() override;
        void deactivate() override;

        void writeToTopic(const string& topic, void* data) override;

    };


}
}
}
}

#endif