#pragma once
#ifdef USE_FASTRTPS

#include <functional>
#include <string>
#include <boost/any.hpp>

#include <fastrtps/TopicDataType.h>

#include "../..//Qos.hpp"
#include "../ModelAdapter.hpp"
#include "../Participant.hpp"
#include "../WriterTopicConfiguration.hpp"

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    using namespace std;

    class FastRTPSWriterTopicConfiguration : public WriterTopicConfiguration {
    public:
        FastRTPSWriterTopicConfiguration(
                const string& _topicName,
                const qos::QosType _qosType,
                const shared_ptr<eprosima::fastrtps::TopicDataType> _dataType
                ) :
            WriterTopicConfiguration(_topicName, _qosType),
            dataType(_dataType)
        {

        }

    public:

        shared_ptr<DataWriter> createWriter(shared_ptr<Participant> participant, const std::string& partition) override;


    private:
        const shared_ptr<eprosima::fastrtps::TopicDataType> dataType;

    };




}
}
}
}
#endif
