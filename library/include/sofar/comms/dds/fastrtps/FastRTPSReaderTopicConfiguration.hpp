#pragma once
#ifdef USE_FASTRTPS

#include <functional>
#include <string>
#include <boost/any.hpp>

#include <fastrtps/TopicDataType.h>

#include "../../Qos.hpp"
#include "../ModelAdapter.hpp"
#include "../Participant.hpp"
#include "../ReaderTopicConfiguration.hpp"
#include "FastRTPSSubsccriptionListener.hpp"

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    using namespace std;

    class FastRTPSReaderTopicConfiguration : public ReaderTopicConfiguration {
    public:
        FastRTPSReaderTopicConfiguration(
                const string& _topicName,
                const qos::QosType _qosType,
                const shared_ptr<eprosima::fastrtps::TopicDataType> _dataType,
                const shared_ptr<FastRTPSSubscribeListenerBase> _listener
                ) :
            ReaderTopicConfiguration(_topicName, _qosType),
            dataType(_dataType),
            listener(_listener)
        {

        }

    public:

        shared_ptr<DataReader> createReader(shared_ptr<Participant> participant, const std::string& partition) override;


    private:
        const std::shared_ptr<FastRTPSSubscribeListenerBase> listener;
        const shared_ptr<eprosima::fastrtps::TopicDataType> dataType;

    };




}
}
}
}
#endif
