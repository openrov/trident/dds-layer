#pragma once

#ifdef USE_FASTRTPS


#include "../DataWriter.hpp"
#include "FastRTPSParticipant.hpp"
#include "FastRTPSSubsccriptionListener.hpp"

#include <boost/any.hpp>

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    class FastRTPSWriter : public DataWriter {
    public:
        FastRTPSWriter(eprosima::fastrtps::Publisher *_publisher, eprosima::fastrtps::Participant *_participant)
                : publisher(_publisher)
                , part(_participant) {}
        ~FastRTPSWriter(){
            // TODO delete on fastrtps
        };

        void writeToTopic(const std::string& topic, void* data) const override;

    protected:
        eprosima::fastrtps::Publisher* publisher;
        eprosima::fastrtps::Participant* part;

    };

}
}
}
}

#endif