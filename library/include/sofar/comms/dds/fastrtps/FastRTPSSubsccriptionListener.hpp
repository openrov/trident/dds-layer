#pragma once
#ifdef USE_FASTRTPS


#include <fastrtps/subscriber/Subscriber.h>
#include <fastrtps/subscriber/SubscriberListener.h>
#include <functional>
#include <fastrtps/subscriber/SampleInfo.h>

#include "../ModelAdapter.hpp"
#include <boost/any.hpp>
#include <spdlog/spdlog.h>

using namespace eprosima::fastrtps::rtps;

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    class FastRTPSSubscribeListenerBase : public eprosima::fastrtps::SubscriberListener {
    public:
        FastRTPSSubscribeListenerBase() = default;
        ~FastRTPSSubscribeListenerBase() = default;
    };

    template<class TDDS, class TModel>
    class   FastRTPSSubscribeListener:public FastRTPSSubscribeListenerBase
    {
    public:
        FastRTPSSubscribeListener(ModelAdapter<TDDS, TModel> _adapter, std::function<void(TModel&)> _newDataCallback)
            :   matched(0),
                samples(0),
                newDataCallback(_newDataCallback ),
                adapter(_adapter){

        };
        ~FastRTPSSubscribeListener() final = default ;

        void onSubscriptionMatched(eprosima::fastrtps::Subscriber* sub, eprosima::fastrtps::rtps::MatchingInfo& info) override {
            (void)sub;
            (void)info;
            spdlog::info("@ [FASTRTPS] Subscription matched for topic {}", sub->getAttributes().topic.topicName);

        }

        void onNewDataMessage(eprosima::fastrtps::Subscriber* sub) override {
            if (newDataCallback != nullptr) {

                if (sub->takeNextData((void *) &data, &sampleInfo)) {
                    spdlog::info("## New Data for topic {} state {}", sub->getAttributes().topic.topicName, sampleInfo.sampleKind);
                    if (sampleInfo.sampleKind == ALIVE) {
                        this->samples++;

                        TModel model = adapter.convert(data);
                        newDataCallback( model );
                    }
                }

            }
        }
        eprosima::fastrtps::SampleInfo_t sampleInfo;
        int matched;
        uint32_t samples;

        TDDS data;
        ModelAdapter<TDDS, TModel> adapter;

    protected:
        std::function<void(TModel&)> newDataCallback;

    };

}
}
}
}
#endif