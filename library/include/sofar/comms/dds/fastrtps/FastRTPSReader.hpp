#pragma once

#ifdef USE_FASTRTPS


#include "..//DataReader.hpp"
#include "FastRTPSParticipant.hpp"
#include "FastRTPSSubsccriptionListener.hpp"

#include <boost/any.hpp>

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    class FastRTPSReader : public DataReader {
    public:
        FastRTPSReader(
                eprosima::fastrtps::Subscriber *_subscriber,
                std::shared_ptr<FastRTPSSubscribeListenerBase>& _listener) :
                subscriber(_subscriber),
                listener(std::move(_listener)) {}
        ~FastRTPSReader(){
            // TODO delete on fastrtps
        };


    protected:
        const eprosima::fastrtps::Subscriber* subscriber;
        std::shared_ptr<FastRTPSSubscribeListenerBase> listener;

    };


}
}
}
}

#endif