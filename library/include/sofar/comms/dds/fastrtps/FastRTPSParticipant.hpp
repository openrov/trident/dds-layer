#pragma once
#ifdef USE_FASTRTPS


#include <memory>
#include <functional>
#include "../../Qos.hpp"
#include "../DataReader.hpp"
#include "../DataWriter.hpp"
#include "../Participant.hpp"
#include "FastRTPSSubsccriptionListener.hpp"
#include "FastRTPSReaderTopicConfiguration.hpp"
#include "FastRTPSWriterTopicConfiguration.hpp"

#include <fastrtps/Domain.h>
#include <fastrtps/participant/Participant.h>
#include <fastrtps/attributes/ParticipantAttributes.h>
#include <fastrtps/subscriber/Subscriber.h>

#include <map>

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    class FastRTPSSubscribeListenerBase;
    class FastRTPSReaderTopicConfiguration;

    class FastRTPSParticipant : public Participant {
    public:
        FastRTPSParticipant( const std::string& participantName, const uint32_t& domainID );
        ~FastRTPSParticipant() override = default;

        std::shared_ptr<DataReader> createReader(
                const std::string &topicName,
                const qos::QosType &qosType,
                const std::string &partition,
                std::shared_ptr<eprosima::fastrtps::TopicDataType> dataType,
                std::shared_ptr<FastRTPSSubscribeListenerBase> listener
                );

        std::shared_ptr<DataWriter> createWriter(
                const std::string &topicName,
                const qos::QosType &qosType,
                const std::string &partition,
                std::shared_ptr<eprosima::fastrtps::TopicDataType> dataType
                );

        const shared_ptr<PortStrategy> createPort(const std::string& partition) override;
        const std::shared_ptr<eprosima::fastrtps::TopicDataType>& getTopicType(const std::type_info& info);

        template<class TDDS, class TTopicType>
        const void registerTopicType() {
            typeMap.insert(
                    std::make_pair(
                            typeid(TDDS).name(),
                            std::static_pointer_cast<eprosima::fastrtps::TopicDataType>( make_shared<TTopicType>())
                    )
            );

        }

        /* Creator function */
        template<class TModel, class TDDS>
        shared_ptr<ReaderTopicConfiguration> makeReaderConfiguration(
                const std::string& topicName,
                const sofar::comms::qos::QosType qosType,
                const std::function<TModel(TDDS)>& adapterCallback,
                std::function<void(TModel&)> _newDataCallback
        ) {

            auto adatper = ModelAdapter<TDDS, TModel>(adapterCallback);
            auto listener = make_shared<FastRTPSSubscribeListener<TDDS, TModel>>(adatper, _newDataCallback);

            return make_shared<FastRTPSReaderTopicConfiguration>(
                    /* topic */ topicName,
                    /* QOS   */ qosType,
                    /* Msg Type */ getTopicType(typeid(TDDS)),
                    /* Listener */ static_pointer_cast<FastRTPSSubscribeListenerBase>(listener)
            );
        }
        template<class TModel, class TDDS>
        shared_ptr<WriterTopicConfiguration> makeWriterConfiguration(
                const std::string& topicName,
                const sofar::comms::qos::QosType qosType) {

            return make_shared<FastRTPSWriterTopicConfiguration>(
                    /* topic */ topicName,
                    /* QOS   */ qosType,
                    /* Msg Type */ getTopicType(typeid(TDDS))
            );
        }


    private:
        eprosima::fastrtps::Subscriber* createSubscriber(
                const std::string &topicName,
                const qos::QosType& qosType,
                const std::string& partition,
                std::shared_ptr<eprosima::fastrtps::TopicDataType> dataType,
                eprosima::fastrtps::SubscriberListener *listener) const;

        eprosima::fastrtps::Publisher* createPublisher(
                const std::string &topicName,
                const qos::QosType& qosType,
                const std::string& partition,
                std::shared_ptr<eprosima::fastrtps::TopicDataType> dataType) const;

        void registerType(const std::shared_ptr<eprosima::fastrtps::TopicDataType> dataType) const;

        vector<string> getPartitions(const string& partition) const;

        eprosima::fastrtps::Participant *participant;

        std::map<const char*, std::shared_ptr<eprosima::fastrtps::TopicDataType>> typeMap;

    };



}
}
}
}
#endif