#pragma once
#ifdef USE_FASTRTPS

#include "FastRTPSParticipant.hpp"
#include "../NodeStrategy.hpp"
#include "../DataReader.hpp"

#include <memory>
#include <map>

using std::unique_ptr;

namespace sofar {
namespace comms {
namespace dds {
namespace fastrtps {

    class FastRTPSNodeStrategy : public NodeStrategy {
    public:
        FastRTPSNodeStrategy(
                const std::string& nodeName,
                const std::vector<std::string>& partitions,
                const int& domain = 0 );

        ~FastRTPSNodeStrategy() = default;

        // Non-copyable
        FastRTPSNodeStrategy( const FastRTPSNodeStrategy& ) = delete;
        FastRTPSNodeStrategy( FastRTPSNodeStrategy&& ) = delete;
        FastRTPSNodeStrategy& operator=( const FastRTPSNodeStrategy& ) = delete;

    };

}
}
}
}

#endif