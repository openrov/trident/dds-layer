#pragma once

#include <vector>
#include <map>

#include "DataReader.hpp"
#include "ReaderTopicConfiguration.hpp"

#include "ReaderTopicConfiguration.hpp"

namespace sofar {
namespace comms {
namespace dds {

    using namespace std;

    class Participant;

    class Port {
    public:
        virtual void activate() = 0;
        virtual void deactivate() = 0;

    protected:
        Port(shared_ptr<Participant> _participant, const std::string& _partition);

        const shared_ptr<Participant> participant;
        const string partition;

    };
}
}
}