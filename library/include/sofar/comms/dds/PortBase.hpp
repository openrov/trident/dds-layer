#pragma once

#include <functional>
#include <vector>
#include <map>

#include "DataReader.hpp"
#include "ReaderTopicConfiguration.hpp"
#include "WriterTopicConfiguration.hpp"

#include "ModelAdapter.hpp"
#include "ReaderTopicConfiguration.hpp"
#include "PortStrategy.hpp"

namespace sofar {
namespace comms {
namespace dds {

    using namespace std;

    class Participant;

    class PortBase {
    public:
        virtual void activate();
//        virtual void deactivate() = 0;

    protected:
        PortBase(shared_ptr<Participant> _participant, shared_ptr<PortStrategy> _portStrategy);
        virtual vector<shared_ptr<ReaderTopicConfiguration>> createReaderTopicConfigurations() {
            return vector<shared_ptr<ReaderTopicConfiguration>>();
        }
        virtual vector<shared_ptr<WriterTopicConfiguration>> createWriterTopicConfigurations() {
            return vector<shared_ptr<WriterTopicConfiguration>>();
        };

        const shared_ptr<Participant> participant;
        vector<shared_ptr<ReaderTopicConfiguration>> readerTopicConfigurations;
        vector<shared_ptr<WriterTopicConfiguration>> writerTopicConfigurations;

        map<shared_ptr<ReaderTopicConfiguration>, shared_ptr<DataReader>> dataReaders;

        vector<shared_ptr<ReaderTopicConfiguration>> getReaderTopicConfigurations();
        vector<shared_ptr<WriterTopicConfiguration>> getWriterTopicConfigurations();

        shared_ptr<PortStrategy> port;


        /* Creator function */
        template<class TModel, class TDDS>
        shared_ptr<ReaderTopicConfiguration> makeReaderConfiguration(
                const std::string& topicName,
                const sofar::comms::qos::QosType qosType,
                const std::function<TModel(TDDS)>& adapterCallback,
                std::function<void(TModel&)> _newDataCallback
        ) {
            return port->makeReaderConfiguration(
                    topicName,
                    qosType,
                    adapterCallback,
                    _newDataCallback
                    );
        }
        /* Creator function */
        template<class TModel, class TDDS>
        shared_ptr<WriterTopicConfiguration> makeWriterConfiguration(
                const std::string& topicName,
                const sofar::comms::qos::QosType qosType
        ) {
            return port->makeWriterConfiguration<TModel, TDDS>(
                    topicName,
                    qosType
                    );
        }


    };
}
}
}