#pragma once

#include <memory>
#include <string>

#include "DataWriter.hpp"
#include "sofar/comms/Qos.hpp"
#include "ModelAdapter.hpp"

#include <boost/any.hpp>

namespace sofar {
namespace comms {
namespace dds {

    using namespace std;
    class Participant;
    class DataReader;

    class WriterTopicConfiguration {
    public:
        WriterTopicConfiguration(const string& _topicName, qos::QosType _qosType)
        :           topicName(_topicName),
                    qualityOfService(_qosType) {

        };

        virtual shared_ptr<DataWriter> createWriter(shared_ptr<Participant> participant, const std::string& partition) = 0;
    public:

        const string& topicName;
        const qos::QosType qualityOfService;
    };


}
}
}