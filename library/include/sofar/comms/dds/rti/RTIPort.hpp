#pragma once
#ifdef USE_RTI


#include <memory>
#include <atomic>
#include <thread>

#include <functional>

#include <dds/dds.hpp>
namespace rtidds = dds;

#include "../Port.hpp"
#include "../PortStrategy.hpp"

namespace sofar {
namespace comms {
namespace dds {
namespace rtix {

    class RTIPort : public PortStrategy {
    public:
        void activate() override;
        void deactivate() override;

        RTIPort(shared_ptr<Participant> _participant, const std::string &partition);

        void writeToTopic(const string& topic, void* data) override;

    private:
        void initThread();
        void terminateThreads();

        std::shared_ptr<rtidds::core::cond::WaitSet> waitSet = make_shared<rtidds::core::cond::WaitSet>();
        std::atomic<bool> isRunning;
        std::thread readerThread;



    };

}
}
}
}
#endif