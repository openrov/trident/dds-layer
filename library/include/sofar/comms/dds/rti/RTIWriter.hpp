#pragma  once

#ifdef USE_RTI


#include <memory>
#include <spdlog/spdlog.h>

#include <dds/dds.hpp>
namespace rtidds = dds;


#include <memory>

#include "../DataWriter.hpp"
#include "RTIReader.hpp"

#include <boost/any.hpp>
#include <sofar/comms/Qos.hpp>

#include "../Participant.hpp"

namespace sofar {
namespace comms {
namespace dds {
    namespace rtix {

        using namespace std;

        class RTIWriterBase : public sofar::comms::dds::DataWriter {
        public:
            RTIWriterBase(shared_ptr<Participant>& _participant)
                    : participant( std::move( _participant ) ) {

            };



        protected:
            rtidds::domain::DomainParticipant* getDomainParticipant();
            rtidds::pub::Publisher* getPublisher(const std::string &_partition);

            const rtidds::pub::qos::DataWriterQos getWriterQosConfiguration(const qos::QosType);

        private:
            shared_ptr<Participant> participant;

        };

        template<class TDDS, class TModel>
        class RTIWriter : public RTIWriterBase {
        public:
            explicit RTIWriter(
                    const std::string &topicName
                    , const qos::QosType &qosType
                    , std::shared_ptr<Participant> _participant
                    , const std::string &_partition
            )
                    : RTIWriterBase(_participant)
                    , partition( _partition )
            {

                auto domainParticipant = getDomainParticipant();
                auto topic = rtidds::topic::find<rtidds::topic::Topic<TDDS>>(*domainParticipant, topicName);
                if (rtidds::core::null == topic) {
                    // Topic did not exist, so create it now
                    topic = rtidds::topic::Topic<TDDS>(*domainParticipant, topicName);
                }

                // Setup the QoS's
                auto qos = getWriterQosConfiguration(qosType);
                ddsWriter = make_shared<rtidds::pub::DataWriter<TDDS>>(*getPublisher(partition), topic, qos);

            }

            void writeToTopic(const std::string& topic, void* data) const override {
                TDDS* dds = (TDDS*)data;
                ddsWriter->write(*dds);
            }

        private:

            const std::string partition;
            shared_ptr<rtidds::pub::DataWriter<TDDS>> ddsWriter;

        };
    }
}
}
}

#endif