#pragma  once

#ifdef USE_RTI


#include <memory>

#include <dds/dds.hpp>
namespace rtidds = dds;


#include <memory>
#include <spdlog/spdlog.h>
#include <boost/any.hpp>

#include "RTIReader.hpp"
#include <sofar/comms/Qos.hpp>

#include "../DataReader.hpp"
#include "../Participant.hpp"

namespace sofar {
namespace comms {
namespace dds {
namespace rtix {

    using namespace std;

    class RTIReaderBase : public sofar::comms::dds::DataReader {
    public:
        RTIReaderBase(shared_ptr<Participant>& _participant)
                : participant( std::move( _participant ) ) {

        };
        void setWaitSet(std::shared_ptr<rtidds::core::cond::WaitSet> _waitSet);

    protected:
        std::shared_ptr<rtidds::core::cond::WaitSet> waitSet = nullptr;
        std::shared_ptr<rtidds::sub::cond::ReadCondition> readCondition;

        rtidds::domain::DomainParticipant* getDomainParticipant();
        rtidds::sub::Subscriber* getSubscriber(const std::string &_partition);

        const rtidds::sub::qos::DataReaderQos getReaderQosConfiguration(const qos::QosType);

    private:
        shared_ptr<Participant> participant;

    };

    template<class TDDS, class TModel>
    class RTIReader : public RTIReaderBase {
    public:
        explicit RTIReader(
                const std::string &topicName
                , const qos::QosType &qosType
                , std::function<TModel(TDDS)> _adapterCallback
                , std::function<void(TModel&)> _newDataCallback
                , std::shared_ptr<Participant> _participant
                , const std::string &_partition
        )
                : RTIReaderBase(_participant)
                , adapterCallback( _adapterCallback )
                , newDataCallback( _newDataCallback )
                , partition( _partition )
        {

            auto domainParticipant = getDomainParticipant();
            auto topic = rtidds::topic::find<rtidds::topic::Topic<TDDS>>(*domainParticipant, topicName);
            if (rtidds::core::null == topic) {
                // Topic did not exist, so create it now
                topic = rtidds::topic::Topic<TDDS>(*domainParticipant, topicName);
            }

            // Setup the QoS's
            auto qos = getReaderQosConfiguration(qosType);

            ddsReader = make_shared<rtidds::sub::DataReader<TDDS>>(*getSubscriber(partition), topic, qos);
            readCondition = make_shared<rtidds::sub::cond::ReadCondition>(
                    *ddsReader,
                    rtidds::sub::status::DataState::any(),
                    [&]() {
                        onNewMessage();
                    }
            );

        }

    private:
        void onNewMessage() {
            // Take all the samples
            try {
                rtidds::sub::LoanedSamples<TDDS> samples = ddsReader->take();
                for(const auto& sample : samples)
                {
                    if(sample.info().valid())
                    {
                        const TDDS foo = sample;
                        TModel res = adapterCallback(foo);
                        newDataCallback(res);
                    }
                }
            }
            catch(const std::exception& err) {
                spdlog::error("Error reading: {}", err.what());
            }

        }

        const std::string partition;
        shared_ptr<rtidds::sub::DataReader<TDDS>> ddsReader;
        std::function<TModel(TDDS)> adapterCallback;
        std::function<void(TModel&)> newDataCallback;

    };
}
}
}
}

#endif