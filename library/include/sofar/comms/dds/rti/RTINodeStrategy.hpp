#pragma once

#ifdef USE_RTI

#include "RTIParticipant.hpp"
#include "../DataReader.hpp"
#include "../NodeStrategy.hpp"

#include <memory>
#include <map>
#include <dds/dds.hpp>
namespace rtidds = dds;

using std::unique_ptr;

namespace sofar {
namespace comms {
namespace dds {
namespace rtix {

    class RTINodeStrategy : public NodeStrategy {
    public:
        RTINodeStrategy(
                const std::string& nodeName,
                const std::vector<std::string>& partitions,
                const int& domain = 0 );

        ~RTINodeStrategy() = default;

        // Non-copyable
        RTINodeStrategy( const RTINodeStrategy& ) = delete;
        RTINodeStrategy( RTINodeStrategy&& ) = delete;
        RTINodeStrategy& operator=( const RTINodeStrategy& ) = delete;

    private:
        const std::vector<std::string> qosFilePaths;
        const std::string defaultLib;
        const std::string defaultProfile;

        rtidds::domain::DomainParticipant          domainParticipant;

        void SetupQos( const std::vector<std::string>& qosFilePaths,
                       const std::string& defaultLib,
                       const std::string& defaultProfile );

    };

}
}
}
}

#endif