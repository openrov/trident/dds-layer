#pragma once
#ifdef USE_RTI

#include <functional>
#include <string>
#include <boost/any.hpp>

#include "../..//Qos.hpp"
#include "../ModelAdapter.hpp"
#include "../Participant.hpp"
#include "../WriterTopicConfiguration.hpp"
#include "RTIWriter.hpp"

namespace sofar {
namespace comms {
namespace dds {
namespace rtix {

    using namespace std;

    template<class TDDS, class TModel>
    class RTIWriterTopicConfiguration : public WriterTopicConfiguration {
    public:
        RTIWriterTopicConfiguration(
                const string& _topicName,
                const qos::QosType _qosType
                ) :
            WriterTopicConfiguration(_topicName, _qosType)
        {

        }

    public:


        shared_ptr<DataWriter> createWriter(shared_ptr<Participant> participant, const std::string& partition) override {
            return std::make_shared<RTIWriter<TDDS, TModel>>(
                    topicName,
                    qualityOfService,
                    participant,
                    partition
            );
        }


    };

}
}
}
}
#endif
