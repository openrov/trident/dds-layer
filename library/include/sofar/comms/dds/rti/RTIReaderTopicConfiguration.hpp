#pragma once
#ifdef USE_RTI

#include <functional>
#include <string>
#include <boost/any.hpp>


#include "../../Qos.hpp"
#include "../ModelAdapter.hpp"
#include "../ReaderTopicConfiguration.hpp"

#include "RTIReader.hpp"

namespace sofar {
namespace comms {
namespace dds {
namespace rtix {

    using namespace std;

    template<class TDDS, class TModel>
    class RTIReaderTopicConfiguration : public ReaderTopicConfiguration {
    public:
        RTIReaderTopicConfiguration(
                const string& _topicName
                , const qos::QosType _qosType
                , std::function<void(TModel&)> _newDataCallback
                , const std::function<TModel(TDDS)>& _adapterCallback
                ) :
            ReaderTopicConfiguration(_topicName, _qosType)//,
            , newDataCallback( _newDataCallback )
            , qosType( _qosType )
            , adapterCallback( _adapterCallback )
        {
            int i = 99;

        }

    public:

        shared_ptr<DataReader> createReader(shared_ptr<Participant> participant, const std::string &partition) override {

            return std::make_shared<RTIReader<TDDS, TModel>>(
                    topicName,
                    qualityOfService,
                    adapterCallback,
                    newDataCallback,
                    participant,
                    partition
                    );
        }

    private:
        std::function<void(TModel&)> newDataCallback;
        std::function<TModel(TDDS)> adapterCallback;
        const qos::QosType qosType;


    };



}
}
}
}
#endif
