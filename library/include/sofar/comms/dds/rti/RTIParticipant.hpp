#pragma once
#ifdef USE_RTI


#include <memory>
#include <functional>
#include "sofar/comms/Qos.hpp"
#include "sofar/comms/dds/DataReader.hpp"
#include "sofar/comms/dds/Participant.hpp"
#include "sofar/comms/dds/ReaderTopicConfiguration.hpp"
#include "sofar/comms/dds/WriterTopicConfiguration.hpp"

#include "RTIReaderTopicConfiguration.hpp"
#include "RTIWriterTopicConfiguration.hpp"
#include "RTIReader.hpp"

#include <dds/dds.hpp>
namespace rtidds = dds;

namespace sofar {
namespace comms {
namespace dds {
namespace rtix{

    class RTIReaderBase;

    using namespace std;

    class RTIParticipant : public Participant {
    public:
        RTIParticipant(
                rtidds::domain::DomainParticipant* _participant,
                rtidds::pub::Publisher pub,
                rtidds::sub::Subscriber sub,
                const std::string& participantName,
                const uint32_t& domainID,
                vector<string> qosFilePaths
                );
        ~RTIParticipant() override = default;

        const shared_ptr<PortStrategy> createPort(const std::string &partition) override;

        rtidds::sub::Subscriber* getSubscriber(const std::string &partition) {
            if ((partition.empty()) || (partition == "*")) {
                return &defaultSubscriber;
            } else {
                auto qos = domainParticipant->default_subscriber_qos();
                if( partition == "*" )
                {
                    qos << rtidds::core::policy::Partition( { "", partition } );
                }
                else
                {
                    qos << rtidds::core::policy::Partition( { partition } );
                }
                return new rtidds::sub::Subscriber( *domainParticipant, qos );
            }

        };

        rtidds::pub::Publisher* getPublisher(const std::string &partition) {
            if ((partition.empty()) || (partition == "*")) {
                return &defaultPublisher;
            } else {
                auto qos = domainParticipant->default_publisher_qos();
                if( partition == "*" )
                {
                    qos << rtidds::core::policy::Partition( { "", partition } );
                }
                else
                {
                    qos << rtidds::core::policy::Partition( { partition } );
                }
                return new rtidds::pub::Publisher( *domainParticipant, qos );
            }

        };

        rtidds::domain::DomainParticipant* getDefaultParticipant() { return domainParticipant; };

        /* Creator function */
        template<class TModel, class TDDS>
        shared_ptr<ReaderTopicConfiguration> makeReaderConfiguration(
                const std::string& topicName,
                const sofar::comms::qos::QosType qosType,
                const std::function<TModel(TDDS)>& adapterCallback,
                std::function<void(TModel&)> _newDataCallback
        ) {
//            auto adapter = ModelAdapter<TDDS, TModel>(adapterCallback);
//            RTISubscriptionListenerBase::Ptr listener =
//                    make_shared<RTISubscriptionListener<TDDS, TModel>>( _newDataCallback, qosType, adapter );

            return make_shared<RTIReaderTopicConfiguration<TDDS, TModel>>(
                    /* topic */ topicName,
                    /* QOS   */ qosType,
                   _newDataCallback,
                   adapterCallback
            );
        }

        template<class TModel, class TDDS>
        shared_ptr<WriterTopicConfiguration> makeWriterConfiguration(
                const std::string& topicName,
                const sofar::comms::qos::QosType qosType) {

            return make_shared<RTIWriterTopicConfiguration<TDDS, TModel>>(
                    /* topic */ topicName,
                    /* QOS   */ qosType
            );
        }
        const rtidds::sub::qos::DataReaderQos getReaderQosConfiguration(const qos::QosType);
        const rtidds::pub::qos::DataWriterQos getWriterQosConfiguration(const qos::QosType);

        vector<string> qosFilePaths;


    private:
        rtidds::domain::DomainParticipant*          domainParticipant;
        rtidds::pub::Publisher                     defaultPublisher;
        rtidds::sub::Subscriber                    defaultSubscriber;


        // Topic Factory Method
        template<typename T>
        static rtidds::topic::Topic<T> CreateTopic( rtidds::domain::DomainParticipant participant, const std::string& topicName )
        {
            // First attempt to find an existing topic with this name and type in the participant
            // TODO: This may not be threadsafe
            auto topic = rtidds::topic::find<rtidds::topic::Topic<T>>( participant, topicName );
            if( rtidds::core::null == topic )
            {
                // Topic did not exist, so create it now
                topic = rtidds::topic::Topic<T>( participant, topicName );
            }

            return topic;
        }

        // Reader Factory method
        template<typename T>
        rtidds::sub::DataReader<T> CreateReader(const std::string& topicName, const std::string& qosString) const {

            // Subscriber
            const auto& subscriber = defaultSubscriber;

            // Get the topic
            rtidds::topic::Topic<T> topic = CreateTopic<T>(domainParticipant, topicName);

            // Setup the QoS's
            const auto& qos = rtidds::core::QosProvider::Default()->datareader_qos(qosString);

            return rtidds::sub::DataReader<T>(subscriber, topic, qos);
        }


    };


}
}
}
}
#endif