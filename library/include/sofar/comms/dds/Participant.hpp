#pragma once

#include <memory>
#include <boost/any.hpp>

#include "sofar/comms/Qos.hpp"
#include "DataReader.hpp"

namespace sofar {
namespace comms {
namespace dds {

    class PortStrategy;
    class NodeStrategy;

    class Participant {
    public:
        Participant() = default;;
        virtual ~Participant() = default;

        virtual const std::shared_ptr<PortStrategy> createPort(const std::string& partition) = 0;

    protected:


        friend class NodeStrategy;
    };

}
}
}