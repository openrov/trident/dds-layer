#pragma once

#include <memory>
#include <string>

#include "DataReader.hpp"
#include "sofar/comms/Qos.hpp"
#include "ModelAdapter.hpp"

#include <boost/any.hpp>

namespace sofar {
namespace comms {
namespace dds {

    using namespace std;
    class Participant;
    class DataReader;

    class ReaderTopicConfiguration {
    public:
        ReaderTopicConfiguration(const string& _topicName, qos::QosType _qosType)
        :           topicName(_topicName),
                    qualityOfService(_qosType) {

        };

        virtual shared_ptr<DataReader> createReader(shared_ptr<Participant> participant, const std::string& partition) = 0;
    public:

        const string& topicName;
        const qos::QosType qualityOfService;
    };


}
}
}