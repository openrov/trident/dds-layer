#pragma once

#include <boost/any.hpp>

namespace sofar {
namespace comms {
namespace dds {

    class DataWriter {
    public:
        ~DataWriter() = default;

        virtual void writeToTopic(const std::string& topic, void* data) const = 0;
    };


}
}
}