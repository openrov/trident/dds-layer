#pragma once

#include <boost/any.hpp>
#include <functional>

namespace sofar {
namespace comms {
namespace dds {

    using namespace std;

    class ModelAdapterBase {
    public:
    };

    template<class TIn, class TOut>
    class ModelAdapter : public ModelAdapterBase {
    public:
        ModelAdapter(
                const std::function<TOut(TIn)> _callback) :
                convertCallback(_callback) {
        };

        ~ModelAdapter() = default;

        TOut convert(TIn in) {
            return convertCallback(in);
        }

    private:
        const std::function<TOut(TIn)>     convertCallback;

    };
}
}
}
