#pragma once

#include <vector>
#include <map>
#include <boost/any.hpp>

#include "Port.hpp"
#include "DataReader.hpp"
#include "ReaderTopicConfiguration.hpp"
#include "WriterTopicConfiguration.hpp"


#ifdef USE_FASTRTPS
#include "sofar/comms/dds/fastrtps/FastRTPSParticipant.hpp"
typedef sofar::comms::dds::fastrtps::FastRTPSParticipant ParticipantImpl;

#else
#include "sofar/comms/dds/rti/RTIParticipant.hpp"
typedef sofar::comms::dds::rtix::RTIParticipant ParticipantImpl;

#endif

namespace sofar {
namespace comms {
namespace dds {

    using namespace std;

    class Participant;

    class PortStrategy : public Port {
    public:
        PortStrategy(shared_ptr<Participant> _participant, const std::string& partition) : Port(_participant, partition) {};
        void setReaderConfigurations(vector<shared_ptr<ReaderTopicConfiguration>> configurations) { readerConfigurations = configurations; }
        void setWriterConfigurations(vector<shared_ptr<WriterTopicConfiguration>> configurations) { writerConfigurations = configurations; }

        virtual void writeToTopic(const string& topic, void* data) = 0;

//        void activate() override;

        /*
         * Creator function fore ReaderTopicConfiguration.
         * As C++ can't do template function that are virtual we have to do a workaround
         * via a typedef and a function in the Participant implementation that has to have the same
         * signature without an interface specification.
         *
         * */

        template<class TModel, class TDDS>
        shared_ptr<ReaderTopicConfiguration> makeReaderConfiguration(
                const std::string& topicName,
                const sofar::comms::qos::QosType qosType,
                const std::function<TModel(TDDS)>& adapterCallback,
                std::function<void(TModel&)> _newDataCallback
        ) {
            return std::static_pointer_cast<ParticipantImpl>(participant)->makeReaderConfiguration(
                    topicName,
                    qosType,
                    adapterCallback,
                    _newDataCallback
                    );

        }

        template<class TModel, class TDDS>
        shared_ptr<WriterTopicConfiguration> makeWriterConfiguration(
                const std::string& topicName,
                const sofar::comms::qos::QosType qosType
            ) {
            return std::static_pointer_cast<ParticipantImpl>(participant)->makeWriterConfiguration<TModel, TDDS>(
                    topicName,
                    qosType
                    );

        }



    protected:
        vector<shared_ptr<ReaderTopicConfiguration>> readerConfigurations;
        vector<shared_ptr<WriterTopicConfiguration>> writerConfigurations;
        map<shared_ptr<ReaderTopicConfiguration>, shared_ptr<DataReader>> readers;
        map<shared_ptr<WriterTopicConfiguration>, shared_ptr<DataWriter>> writers;

    };
}
}
}