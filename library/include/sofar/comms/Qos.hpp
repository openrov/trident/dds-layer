#pragma once

namespace sofar {
namespace comms {

// Common QOS Profile Names
namespace qos {
    // Participant Level
    constexpr auto PARTICIPANT_COMMON = "Trident::Participant.Common";
    constexpr auto PARTICIPANT_VEHICLE = "Trident::Participant.Vehicle";

    // Reader/Writer Level
    constexpr auto BEST_EFFORT = "Trident::Generic.BestEffort";
    constexpr auto KEEP_LAST_RELIABLE = "Trident::Generic.KeepLastReliable";
    constexpr auto KEEP_LAST_RELIABLE_TRANSIENT_LOCAL = "Trident::Generic.KeepLastReliable.TransientLocal";
    constexpr auto LIVE_VIDEO = "Trident::LiveVideo";

    enum class QosType {
        BestEffort,
        KeepLastReliable,
        KeepLastReliableTransientLocal,
//        LiveVideo,

    };
}
}
}