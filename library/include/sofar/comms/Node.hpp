 #pragma once

#include <string>
#include <vector>
#include <memory>
#include <functional>

#include "sofar/comms/Qos.hpp"
#include "sofar/comms/dds/NodeStrategy.hpp"
#include "sofar/comms/dds/Participant.hpp"
#include "sofar/comms/dds/DataReader.hpp"

#ifdef USE_FASTRTPS
#include "sofar/comms/dds/fastrtps/FastRTPSNodeStrategy.hpp"
#else
#include "sofar/comms/dds/rti/RTINodeStrategy.hpp"
#endif

namespace sofar {
namespace comms {

    class Node
    {
    public:
        Node(   const std::string& nodeName,
                const std::vector<std::string>& defaultPartitions,
                int domain = 0,
                std::function<void(std::shared_ptr<sofar::comms::dds::Participant>)> participantInitCallback = [](std::shared_ptr<sofar::comms::dds::Participant> _) {} );

        ~Node() = default;

        // Non-copyable
        Node( const Node& ) = delete;
        Node( Node&& ) = delete;
        Node& operator=( const Node& ) = delete;

         // Getters
         const std::string&                      Name() const { return nodeName; };

         std::shared_ptr<sofar::comms::dds::Participant>         defaultParticipant() const;

        private:
            const std::string                           nodeName;
#ifdef USE_FASTRTPS
            const dds::fastrtps::FastRTPSNodeStrategy   nodeStrategy;
#else
            const dds::rtix::RTINodeStrategy            nodeStrategy;
#endif
    };

}
}