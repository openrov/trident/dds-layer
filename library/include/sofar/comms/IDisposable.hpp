#pragma once

namespace sofar {
namespace comms {

    class IDisposable {
    public:
        virtual void dispose() = 0;
    };

}
}