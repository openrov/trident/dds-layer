from cpt.packager import ConanMultiPackager

builder = ConanMultiPackager()

builder.auth_manager.login( "orov_private" )

builder.add(settings={"build_type": "Debug"}, options={ "dds": "rti" }, env_vars={}, build_requires={})
builder.add(settings={"build_type": "Release"}, options={ "dds": "rti" }, env_vars={}, build_requires={})

builder.add(settings={"build_type": "Debug"}, options={ "dds": "fastrtps" }, env_vars={}, build_requires={})
builder.add(settings={"build_type": "Release"}, options={ "dds": "fastrtps" }, env_vars={}, build_requires={})

builder.run()