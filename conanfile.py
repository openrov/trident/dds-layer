from conans import ConanFile, CMake

FASTRTPS="fastrtps"
RTI="rti"

class OrovcoreConan( ConanFile ):
    name            = "sofar-dds-layer"
    version         = "0.1.0"
    license         = "Apache 2.0"
    url             = "https://gitlab.com/openrov/trident/dds-layer"
    description     = "DDS Abstraction layer to simplify access to DDS implementations"

    exports_sources = "library/*", "CMakeLists.txt", "LICENSE.txt"
    settings        = "os", "arch", "compiler", "build_type", "arch_build"

    requires        = (
                    )

    build_requires  = (
                        ( "spdlog/1.3.1@bincrafters/stable" ),
                        ( "boost/1.70.0@conan/stable" ),
                      )
    options         = { "dds": [ RTI, FASTRTPS ] }
    default_options = { "dds": FASTRTPS }

    generators      = "cmake"

    def build_requirements(self):
        if self.options.dds == RTI:
            self #noop
        elif self.options.dds == FASTRTPS:
            self.options["fastrtps"].shared = False
            self.options["OpenSSL"].shared = False

        else: 
            raise ConanInvalidConfiguration("Invalid value for option dds: " + self.options.dds)

    def configure(self):
        if self.options.dds == RTI:
            self.requires("rticonnextpro/6.0.0@openrov/stable")
            
        elif self.options.dds == FASTRTPS:
            self.requires("fastrtps/1.9.0-b2-trident-1@openrov/testing")
            self.requires("foonathan-memory/0.6.1.a@openrov/stable")


        else: 
            raise ConanInvalidConfiguration("Invalid value for option dds: " + self.options.dds)



    def build( self ):
        cmake = CMake(self)
        if self.options.dds == RTI:
            cmake.definitions["USE_RTI"] = "ON"

        elif self.options.dds == FASTRTPS:
            cmake.definitions["USE_FASTRTPS"] = "ON"

        # if self.options.shared:
        # cmake.definitions["BUILD_SHARED_LIBS"] = "ON"
        # cmake.definitions["CMAKE_CXX_FLAGS"] = "-fPIC"
        # else:
        #     cmake.definitions["BUILD_SHARED_LIBS"] = "OFF"


        cmake.configure()
        cmake.build()
    
    def package( self ):
        self.copy( "library/include/*", dst="", keep_path=True )
        self.copy( "*.a", dst="lib", keep_path=False )
        self.copy( "*LICENSE.txt", dst="licenses", keep_path=False )

    def package_info( self ):
        self.cpp_info.includedirs   = [ 'library/include' ]
        self.cpp_info.libdirs       = [ 'lib' ]

        self.cpp_info.libs          = [ 'sofar-dds-layer', 'boost_system', 'boost_filesystem' ]
